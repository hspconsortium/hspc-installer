# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    config.vm.hostname = "hspc-reference-platform"
    config.vm.box = "bento/ubuntu-16.04"

    # mysql_database
    config.vm.network :forwarded_port, guest: 3306, host: 3307

    # auth
    config.vm.network :forwarded_port, guest: 9060, host: 9060
    # account
    config.vm.network :forwarded_port, guest: 9065, host: 9065
    # account_api
    config.vm.network :forwarded_port, guest: 9066, host: 9066

    # api_dstu2
    config.vm.network :forwarded_port, guest: 9071, host: 9071
    # api_stu3_v2
    config.vm.network :forwarded_port, guest: 9072, host: 9072
    # api_stu3_v3
    config.vm.network :forwarded_port, guest: 9073, host: 9073
    # api_stu3
    config.vm.network :forwarded_port, guest: 9074, host: 9074

    # sandbox_manager
    config.vm.network :forwarded_port, guest: 9080, host: 9080
    # sandbox_manager api
    config.vm.network :forwarded_port, guest: 13000, host: 13000

    # gallery
    config.vm.network :forwarded_port, guest: 9085, host: 9085
    # bilirubin-risk-chart
    config.vm.network :forwarded_port, guest: 9086, host: 9086
    # bilirubin-monitor
    config.vm.network :forwarded_port, guest: 9087, host: 9087
    # bilirubin_cds_hooks
    config.vm.network :forwarded_port, guest: 9088, host: 9088

    # messaging
    config.vm.network :forwarded_port, guest: 9091, host: 9091
    # apps
    config.vm.network :forwarded_port, guest: 9093, host: 9093

    # patient_picker
    config.vm.network :forwarded_port, guest: 9094, host: 9094
    # appointments
    config.vm.network :forwarded_port, guest: 9095, host: 9095
    # patient_data_manager
    config.vm.network :forwarded_port, guest: 9096, host: 9096

    config.vm.provider "virtualbox" do |vb|
        vb.name = "HSPC Reference Platform"
        vb.memory = "3072"
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        vb.customize ["modifyvm", :id, "--cableconnected1", "on"]
    end

    config.vm.provision "fix-no-tty", type: "shell" do |s|
        s.privileged = false
        s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
    end

    config.vm.provision "ansible" do |ansible|
        ansible.verbose=""
        ansible.compatibility_mode="2.0"
        ansible.playbook = "site.yml"
        # append to the auto-generated inventory file
        ansible.extra_vars = {
            env: "local"
        }
    end

    # If you are running the build on a Windows host, please comment out the
    # "ansible" provisioner above and use this "shell" provisioner:
    #
    # config.vm.provision "shell", path: "provisioning/provision-on-guest.sh"

end
